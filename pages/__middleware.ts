import { NextResponse, NextRequest, NextFetchEvent } from "next/server";

// Re-route path / to /notification by default
export async function middleware(req: NextRequest, ev: NextFetchEvent) {
  const { pathname } = req.nextUrl;

  if (pathname == "/") {
    return NextResponse.redirect(`/hello`);
  }

  return NextResponse.next();
}
