import { useRouter } from 'next/router'
import en from './en.json';
import fr from './fr.json';


const useTrans = () => {
    const { locale } = useRouter();
    const languages: {[key: string]: any;} = {
      'en': en,
      'fr': fr,
    };
    return locale ? languages[locale] : ''
}

export default useTrans;